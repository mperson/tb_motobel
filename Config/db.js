const tedious = require ('tedious');
const { Sequelize } = require('sequelize');

const { dbName, dbConfig}= require("../config.json");

module.exports=db={}

initialize();

async function initialize()
{
    const dialect="mssql";
    const host= dbConfig.server
    const { userName, password} = dbConfig.authentication.options;

    //CONNECT ME :p
    const sequelize = new Sequelize(dbName,userName,password,
        {
            "host": host,
            "dialect": dialect
        });

    //intialization des models
    db.Produit= require("../Models/ms/produit.model")(sequelize);
    
    db.User= require("../Models/ms/user.model")(sequelize);
 
    //await sequelize.sync({ alter:false});
    
}