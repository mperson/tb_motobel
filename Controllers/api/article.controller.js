const { Request, Response } = require('express');
const produitService= require("../../Services/produit.service") ;

const articleController = 
{
    /**
     * Méthode get du api article controller
     * @param {Request} req La requete
     * @param {Response} res La reponse
     */
    get: async(req, res) => 
    {        
       let produits = await produitService.getAll();       
        
        res.json(produits);
    },

    /**
     * Méthode get du api article controller
     * @param {Request} req La requete
     * @param {Response} res La reponse
     */
     getById: async(req, res) => 
     {        
        let produits = await produitService.getAll();       
         
         res.json(produits);
     },

     /**
     * Méthode get du api article controller
     * @param {Request} req La requete
     * @param {Response} res La reponse
     */
      getById: async(req, res) => 
      {        
         let produits = await produitService.getById(req.params.id);   
          
          res.json(produits);
      },
      /**
     * Méthode post du api article controller
     * @param {Request} req La requete
     * @param {Response} res La reponse
     */
       create: async(req, res) => 
       {        
           
       }
 
}

module.exports = articleController;