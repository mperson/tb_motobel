const { Request, Response } = require('express');
const userService= require("../../Services/user.service") ;
const User= require("../../Models/ms/user.model") ;

const accountController = 
{
    /**
     * Méthode de login
     * @param {Request} req La requete
     * @param {Response} res La reponse
     */
    login: async(req, res) => 
    {        
        let { Login, Password } = req.body; 
        let userT = await userService.login(Login, Password);       
        if(userT==null) res.sendStatus(401);
        res.json(userT);
    },
    /**
     * Méthode d'enregistrement
     * @param {Request} req La requete
     * @param {Response} res La reponse
     */
     register: async(req, res) => 
     {    
        try 
        {
            let userT = await userService.register(req.body);       
            res.sendStatus(201);
        } catch (error) {
            console.log(error);
            let message = error ;
           res.sendStatus(404,{"error":message}); 
        }     
        
     },

     
 
}

module.exports = accountController;