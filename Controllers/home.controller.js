const { Request, Response } = require('express');
const produitService= require("../Services/produit.service") ;

const homeController = 
{
    /**
     * Méthode index du home controller
     * @param {Request} req La requete
     * @param {Response} res La reponse
     */
    index: async(req, res) => 
    {        
       let produits = await produitService.getAll();       
        console.log("index");
        res.render('Home/Index', {"produits":produits});
    }
 
}

module.exports = homeController;