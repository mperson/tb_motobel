const db = require("../Config/db");
const bcrypt = require("bcrypt");
const userToken = require("../Models/dto/userToken.dto"); 
const {  ACCESSTOKENSECRET } = process.env;
const jwt = require("jsonwebtoken");
module.exports = 
{
   register,
   login 
}

async function register(newuser){

    const salt = await bcrypt.genSalt(10);
    // now we set user password to hashed password
    newuser.Password = await bcrypt.hash(newuser.Password, salt);
 
    //create And save user
    await db.User.create(newuser);
      
}
async function login(login,password)
{ 
    let existingUser = await db.User.findAll({where:{Login:login}});
    if(!existingUser) throw "User non trouvé";
    else
    { 
        //vérifier le password
        console.log(existingUser[0]);
        const validPassword = await bcrypt.compare(password, existingUser[0].Password);
        if(!validPassword) return null;
        try 
        {    
                let token  =jwt.sign({ user_id: existingUser[0].id, Name: existingUser[0].Login },ACCESSTOKENSECRET,{ expiresIn: "1h" });
                let usertokenDto = new userToken(existingUser[0].IdUser, login,token);
                return usertokenDto;
           
        } catch (err) {
            console.log(err); 
            return null;
        }
    }
    
}
