USE [master]
GO
/****** Object:  Database [MotoBel]    Script Date: 08-11-22 20:47:01 ******/
CREATE DATABASE [MotoBel]
GO
USE [MotoBel]
GO
/****** Object:  User [ExpressUser]    Script Date: 08-11-22 20:47:02 ******/
CREATE USER [ExpressUser] FOR LOGIN [ExpressUser] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_datareader] ADD MEMBER [ExpressUser]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [ExpressUser]
GO
/****** Object:  Table [dbo].[Produit]    Script Date: 08-11-22 20:47:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Produit](
	[IdProduit] [int] IDENTITY(1,1) NOT NULL,
	[Nom] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[Photo] [nvarchar](50) NOT NULL,
	[EstDisponible] [bit] NOT NULL,
	[Prix] [float] NOT NULL,
 CONSTRAINT [PK_Produit] PRIMARY KEY CLUSTERED 
(
	[IdProduit] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Produit] ON 
GO
INSERT [dbo].[Produit] ([IdProduit], [Nom], [Description], [Photo], [EstDisponible], [Prix]) VALUES (1, N'Triuph Trident 660', N'Belle moto', N'triumph.jpg', 1, 8450)
GO
INSERT [dbo].[Produit] ([IdProduit], [Nom], [Description], [Photo], [EstDisponible], [Prix]) VALUES (2, N'Kawazaki Z650', N'aniversary Edition', N'kawa.jpg', 1, 8450)
GO
SET IDENTITY_INSERT [dbo].[Produit] OFF
GO
USE [master]
GO
ALTER DATABASE [MotoBel] SET  READ_WRITE 
GO
