const express= require("express"); 

const homeController = require('../Controllers/home.controller');
const htmlRouter = express.Router();


htmlRouter.get('/', homeController.index);



module.exports=htmlRouter;