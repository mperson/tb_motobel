const express= require("express");
const articleController = require('../Controllers/api/article.controller'); 
const accountController = require("../Controllers/api/account.controller");
const apiRouter = express.Router();
/*Middleware*/
const authenticateJWT = require("../Middleware/authJwt.middleware");


/*api*/
//GET
apiRouter.get("/articles", articleController.get);
apiRouter.get("/articles/:id", articleController.getById);

//POST
apiRouter.post("/articles", articleController.create);
apiRouter.post("/account/login", accountController.login );
apiRouter.post("/account/register", accountController.register ); 

module.exports=apiRouter;