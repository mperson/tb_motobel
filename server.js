
// Chargement des variables d'env
require('dotenv-flow').config();
const { PORT,NODE_ENV, ACCESSTOKENSECRET } = process.env;
var cors = require('cors')
const express= require('express');  
const swaggerUi = require('swagger-ui-express')
const swaggerFile = require('./swagger_output.json')
const htmlRouter = require("./Routes/defaultRoutes.config");
const apiRouter = require("./Routes/apiRoutes.config");

const app= express();
//enable cors
app.use(cors())
// - Fichier static
app.use(express.static('public')); 
app.use(express.urlencoded({ extended: true }));
//Pour le json
app.use(express.json());

app.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerFile))
app.use('/api',apiRouter);
app.use('/',htmlRouter);

// Config du moteur de vue
app.set('view engine', 'pug');
app.set('views', 'views');

app.listen(PORT, () => {
    console.log(`Server up on port ${PORT} in mode ${NODE_ENV}`);
})