const swaggerAutogen = require('swagger-autogen')();

const outputFile = './swagger_output.json';
const endpointsFiles = ['./Routes/apiRoutes.config.js'];

swaggerAutogen(outputFile, endpointsFiles);