const { DataTypes } = require ("sequelize");

function model(sequelize)
{
    const attributes =
    {
        IdProduit: { type: DataTypes.INTEGER, allowNull:false, autoIncrement: true,
            primaryKey: true},
        Nom: { type: DataTypes.STRING, allowNull:false},
        Description: { type: DataTypes.STRING, allowNull:false},
        Photo:{ type: DataTypes.STRING, allowNull:false},
        EstDisponible:{ type:DataTypes.BOOLEAN, allowNull:false},
        Prix: {type:DataTypes.FLOAT, allowNull:false}
    }
    const options = 
    {
        timestamps: false,
        tableName: 'Produit'
    }
    return sequelize.define("Produit", attributes, options);
}

module.exports=model;