const { DataTypes } = require ("sequelize");

function model(sequelize)
{
    const attributes =
    {
        IdUser: { type: DataTypes.INTEGER, allowNull:false, autoIncrement: true,
            primaryKey: true},
        Login: { type: DataTypes.STRING, allowNull:false},
        Password: { type: DataTypes.STRING, allowNull:false},
    }
    const options = 
    {
        timestamps: false,
        tableName: 'User'
    }
    return sequelize.define("User", attributes, options);
}

module.exports=model;